#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define HEAPSIZE 10000 //10kb


typedef struct heapBlock {
	size_t size;
	int free = 1;
	heapBlock* next;
} heapBlock;


char heap[HEAPSIZE];//10000 bytes = 10kb of heap

void *myMalloc(size_t size);
void *myCalloc(size_t size);
void *myRealloc(void* origPtr, size_t size);
void myFree(void *ptr);
void splitBlock(heapBlock* origBlock, size_t size);
void initMalloc();
void defragment();


heapBlock *heapPtr = (void *)heap;//creates a pointer at the beginning of the heap

int main(int argc, char const *argv[])
{
	//test case

	int n, i, *ptr, sum = 0;

    printf("Enter number of elements: ");
    scanf("%d", &n);

    ptr = (int*) myMalloc(n * sizeof(int));
 
    // if memory cannot be allocated
    if(ptr == NULL)                     
    {
        printf("Error! memory not allocated.");
        exit(0);
    }

    printf("Enter elements: ");
    for(i = 0; i < n; ++i)
    {
        scanf("%d", ptr + i);
        sum += *(ptr + i);
    }

    printf("Sum = %d", sum);
  
    // deallocating the memory
    myFree(ptr);
	
	return 0;
}

void myFree(void *ptr){
	//check to see if ptr is in heap range
	heapBlock *blockPtr = ptr + sizeof(heapBlock);
	blockPtr->free = 1;
	defragment(); 
}


//initializes our Malloc
void initMalloc(){
	heapPtr->size = HEAPSIZE - sizeof(heapBlock);
	heapPtr->free = 1;
	heapPtr->next = NULL;
}

//iterates through the heap and joins consecutively free blocks to reduce heap(memory) fragmentation
void defragment(){
	heapBlock *curr = blockPtr; 
	heapBlock *trailPtr = curr;
	while(curr != NULL){

		if((curr->next != NULL) && ((curr->next->free == 1) && (curr->->free == 1))){
			trailPtr = curr->next;
			while((trailPtr != NULL) && (trailPtr->free==1)){
				curr->next = trailPtr->next;
				trailPtr = trailPtr->next;
			}
			curr = trailPtr;
		}else{
			curr=curr->next;
		}

	}
	
}

//allocates memory from the heap of the give size
void myMalloc(size_t size){
	heapBlock *curr;
	curr = heapPtr;
	if(heapPtr == NULL){
		initMalloc();
	}

	if(size > heap){
		return NULL;
	}

	//iterate over blocks to find free block
	while(curr->next != NULL){
		if((curr->size>=size) && (curr->free==1)){
			if(curr->size>size){
				splitBlock(curr, size);
				return (void *) (curr+sizeof(heapBlock)); //give current block start after metadata
			}else{
				curr->free=0;
				return (void *) (curr+sizeof(heapBlock)); //give current block start after metadata
			}
		}

		curr = curr->next;
	}

	return NULL;//no null value 
}

void *myCalloc(size_t size){
	void *ptr = myMalloc(size);
	int fillVal = 0;
	if(ptr == NULL){
		return NULL;
	}

	memset(ptr, fillVal, size);

	return ptr;
}

void *myRealloc(void* origPtr, size_t size) {
	void *newPtr = myMalloc(size);
	memcpy(newPtr, origPtr,origPtr->size);//copies contents of origPtrs block to the newPtr location
	myFree(origPtr);
	return newPtr;
}

//divides a large block in the heap to the correct size provided by the parameter  
void splitBlock(heapBlock *origBlock, size_t size){
	heapBlock *new = (void *)((void *)origBlock+sizeof(heapBlock)+size);
	new->size = origBlock->size-(size+sizeof(heapBlock));
	new->free = 1;
	origBlock->free = 0;
	origBlock->next = new;
	heapBlock->size = size;
}
